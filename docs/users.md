# Categorías de usuarios

En MaadiX existen diferentes categorías de usuarios que puedes activar o administrar en tu sistema. Cada una cuenta con distintos privilegios y funciones, que en esta página te ayudaremos a entender para saber mejor cómo utilizarlas.


* **Administrador del panel de control**: se trata de la única cuenta con privilegios ilimitados para la administración de los datos a través del panel de control. Esta cuenta se crea automáticamente en el mismo momento en el que se crea el servidor, y no se puede eliminar.
* **Superusuario**: aunque esta cuenta no tenga acceso a través del panel de control, es jerárquicamente más importante que la anterior, dado que tiene privilegios ilimitados sobre la totalidad del sistema ("usuario root" de Linux). No se puede eliminar, de modo que no llegues a perder nunca el acceso root al sistema.
* **Usuarios ordinarios**: cuentas que el administrador puede crear a través del panel de control, a las cuales puede otorgar permisos para utilizar o acceder a los distintos servicios.    
* **Usuario postmaster**: por cada dominio activado en el panel de control, se crea una cuenta postmaster. Este tipo de cuenta tendrá la posibilidad de crear y administrar cuentas de correo electrónico asociadas al mismo dominio. 
* **Cuentas de correo**: las cuentas de correo electrónico tendrán acceso al panel de control con el único privilegio de ver o editar sus propios datos (contraseña, nombre, reenvío autómatico...).  

## Administrador

El administrador del panel de control es probablemente la cuenta que más utilizarás, ya que tiene todos los privilegios dentro del panel de control. Esto significa que esta cuenta puede crear, configurar y eliminar a todas las demás así como los dominios, las cuentas de correo electrónico y cualquier otro dato administrable desde el panel de control.

Esta cuenta se crea automáticamente y no es posible eliminarla. De lo contrario, perderías el acceso al panel de control.  
Cuando desde MaadiX construimos el servidor, también creamos esta cuenta, asignándole una contraseña aleatoria que por razones de seguridad tendrás que cambiar la primera vez que accedes al panel de control.

Todos los parámetros, excepto el nombre de usuario, se pueden editar en cualquier momento desde la página de 'Perfil' del panel de control. Puedes acceder a ella a través del menú que se despliega en la esquina superior derecha.  

![admin](../img/es/users/edit-profile.png)

Es muy importante que el correo electrónico asociado a esta cuenta sea válido y que tengas acceso a él, puesto que es el sistema lo utiliza para enviar ciertas notificaciones o funcionalidades importantes, tales como la recuperación de la contraseña.


  
## Superusuario

Esta cuenta no puede operar desde el panel de control (de hecho, ni siquiera tiene un acceso válido al mismo). Aún así, es la cuenta más poderosa del sistema. Técnicamente hablando, es el usuario 'root'.  
Tener acceso ´root´ a un sistema significa tener el control total sobre el mismo, poder implementar cualquier tipo de cambio, acceder a cualquier carpeta e instalar cualquier aplicación desde la consola.  

 
Por razones de seguridad, es aconsejable limitar el uso de esta cuenta cuando sea posible, creando en su lugar cuentas de usuarios ordinarios. En este mismo documento encontrarás más detalles sobre cómo crear y utilizar este tipo de cuentas.  

 
Al igual que el administrador, el Superusuario se crea automáticamente en el proceso de creación del servidor, y hasta que no cambies su contraseña durante el proceso de activación del panel de dontrol, será una cuenta inactiva, sin ningún tipo de acceso.  

Una vez activada, esta cuenta podrá acceder al servidor a través de una conexión SFTP o SSH. Sus privilegios en el sistema son ilimitados, de forma que le permiten efectuar cualquiera de las siguientes operaciones, que no están permitidas desde el panel de control:

* Acceder a todas las carpetas del sistema a través una conexión (STFP/SSH).
* Leer, modificar, eliminar o ejecutar cualquier archivo del sistema incluso si no es propietario (SSH).
* Modificar, instalar o eliminar cualquier aplicación (SSH).
* Crear otras cuentas`root`.
* Apagar o reiniciar el servidor.


## Usuarios ordinarios

Son cuentas creadas por el administrador y a las que se pueden asignar diferentes grados de permisos y accesos.

* **Acceso SFTP**: podrán acceder al servidor por SFTP y estarán confinadas en su propia carpeta personal. No pueden acceder al resto del sistema ni tienen acceso SSH.
* **Webmaster**: pueden ser nombradas webmasters de una determinada aplicación web. En este caso, se crea un acceso directo en su carpeta personal a la carpeta de la aplicación web. Se convierte en la propietaria de dicha carpeta y de todos los archivos que pueda contener, adquiriendo así todos los privilegios sobre estos archivos. Ten presente que las cuentas tipo webmaster necesitarán tener activado el acceso SFTP para poder editar los archivos.   
* **Cuenta VPN**: se les puede activar una cuenta VPN para que su conexión con el servidor sea directa y segura. Asimismo,  podrán utilizar esta conexión para conectarse a cualquier otra dirección de Internet.
* **Acceso aplicación phpMyAdmin**: se les puede activar el permiso para acceder a esta aplicación, en caso de que estuviera instalada. Por razones de seguridad, esta aplicación está protegida por una doble contraseña y es necesario tener habilitado el acceso a la interfaz con tal de poder acceder.

## Usuarios postmaster

Esta cuenta solamente tiene la facultad de administrar las cuentas de correo electrónico asociadas a su dominio. Podrá crear, modificar y borrar las direcciones de correo a través del panel de control. Este tipo de cuenta es sumamente útil para poder permitir estas tareas sin necesidad de otorgar a otra persona todos los privilegios.  

Las cuentas postmaster se crean por defecto por cada dominio que se habilita en el panel de control con una contresaneña aleatoria que se puede editar a posteriori.


Para operar desde el panel de control, las cuentas postmaster tendrán que identificarse utilizando el nombre de usuario postmaster@sudominio.com y la contraseña que se le ha asignado.  

![Postmaster log in](../img/es/users/postmaster-login.png)

  
Una vez dentro del panel de control, las cuentas postmaster tendrán una interfaz ligeramente distinta a la del administrador, en la que sólo tendrán habilitado el acceso a las funcionalidades de edición de las cuentas de correos asociadas a su dominio así como la edición de su propio perfil..

![Postmaster logged in](../img/es/users/postamster-logged.png)


## Cuentas de correo

Las cuentas de correo electrónico creadas en el panel de control pueden ser administradas directamente por sus titulares.  
La persona que tenga un correo electrónico activado, podrá entrar en el panel de control insertando su cuenta de correo como nombre de usuario y la contraseña asignada a la misma.     

![Email log in](../img/es/users/email-login.png)  

Sus privilegios de edición están limitados a su propia cuenta de correo electrónico. Podrá cambiar la contraseña y su nombre y apellido, además de establecer o desactivar el reenvío y las respuesta automática.   
También puede acceder para consultar los datos para la configuración de su cuenta en un cliente de correo electrónico en su dispositivo (Thunderbird, Outlook...).   

![Email logged in](../img/es/users/email-logged.png)  


